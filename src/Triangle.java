public class Triangle {

    public Triangle() {}

    public int triangleType(int a, int b, int c) {
        if (a <= 0 || b <= 0 || c <= 0) {
            return 0; //"Invalid";
        }
        if (a == b && b == c) {
            return 1; //"Equilateral";
        }
        if (a == b || b == c || a == c) {
            return 2; //"Isosceles";
        }
        return 3; //"Scalene";
    }
}

    //compare 2 triangles
