public class Calculator {
    public Calculator() {}

    public int soma(int num1, int num2) {
        return (num1+num2);
    }

    public int multipl(int num1, int num2) {
        return (num1*num2);
    }

    public int bigger(int num1, int num2) {
        return Math.max(num1,num2);
    }
}
