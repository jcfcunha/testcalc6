import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TriangleTest {

    @Test
    void triangleType() {
        Triangle t = new Triangle();
        assertEquals(3, t.triangleType(4, 5, 6));
        assertEquals(0, t.triangleType(0, 1, 1));
        assertEquals(0, t.triangleType(1, 2, -3));
      /*  assertEquals("Invalid", t.triangleType(1, 1, 0));
        assertEquals("Invalid", t.triangleType(1, 2, 3));
        assertEquals("Invalid", t.triangleType(1, 3, 2));
        assertEquals("Invalid", t.triangleType(3, 1, 2));
        assertEquals("Invalid", t.triangleType(-1, -1, -1));
        assertEquals("Invalid", t.triangleType(-1, 1, 1));
        assertEquals("Invalid", t.triangleType(1, -1, 1));
        assertEquals("Invalid", t.triangleType(1, 1, -1));
        assertEquals("Invalid", t.triangleType(-1, -2, -3));
        assertEquals("Invalid", t.triangleType(-1, -3, -2));
        assertEquals("Invalid", t.triangleType(-3, -1, -2));
        assertEquals("Invalid", t.triangleType(1, 2, -3));
        assertEquals("Invalid", t.triangleType(1, -2, 3));
        assertEquals("Invalid", t.triangleType(-1, 2, 3));
        assertEquals("Invalid", t.triangleType(1, 3, -2));
        assertEquals("Invalid", t.triangleType(-1, 3, 2));
        assertEquals("Invalid", t.triangleType(3, -1, 2));
        assertEquals("Invalid", t.triangleType(3, 1, -2));
        assertEquals("Invalid", t.triangleType(2, -1, 3));
        assertEquals("Invalid", t.triangleType(2, 3, -1));
        assertEquals("Invalid", t.triangleType(-2, 1, 3));
        assertEquals("Invalid", t.triangleType(-2, 3, 1));
        assertEquals("Invalid", t.triangleType(3, -2, 1));
        assertEquals("Invalid", t.triangleType(3, 1, -2));
        assertEquals("Invalid", t*/
    }
}