import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TriangleTypeTest {

    @Test
    void triangleType() {
        TriangleType t = new TriangleType();
        assertEquals("Invalid", t.triangleType(0, 0, 0));
        assertEquals("Invalid", t.triangleType(0, 1, 1));
        assertEquals("Invalid", t.triangleType(1, 0, 1));
        assertEquals("Invalid", t.triangleType(1, 1, 0));
    /*    assertEquals("Invalid", t.triangleType(1, 2, 3));
        assertEquals("Invalid", t.triangleType(1, 3, 2));
        assertEquals("Invalid", t.triangleType(3, 1, 2));
        assertEquals("Invalid", t.triangleType(-1, -1, -1));
        assertEquals("Invalid", t.triangleType(-1, 1, 1));
        assertEquals("Invalid", t.triangleType(1, -1, 1));
        assertEquals("Invalid", t.triangleType(1, 1, -1));
        assertEquals("Invalid", t.triangleType(-1, -2, -3));
        assertEquals("Invalid", t.triangleType(-1, -3, -2));
        assertEquals("Invalid", t.triangleType(-3, -1, -2));
        assertEquals("Invalid", t.triangleType(1, 2, -3));
        assertEquals("Invalid", t.triangleType(1, -2, 3));
        assertEquals("Invalid", t.triangleType(-1, 2, 3));
        assertEquals("Invalid", t.triangleType(1, 3, -2));
        assertEquals("Invalid", t.triangleType(-1, 3, 2));
        assertEquals("Invalid", t.triangleType(3, -1, 2));
        assertEquals("Invalid", t.triangleType(3, 1, -2));
        assertEquals("Invalid", t.triangleType(2, -1, 3));
        assertEquals("Invalid", t.triangleType(2, 3, -1));
        assertEquals("Invalid", t.triangleType(-2, 1, 3));
        assertEquals("Invalid", t.triangleType(-2, 3, 1));
        assertEquals("Invalid", t.triangleType(3, -2, 1));
        assertEquals("Invalid", t.triangleType(3, 1, -2));*/
    }
}