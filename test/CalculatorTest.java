import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    Calculator s;

    @BeforeEach
    void setUp() {
        s=new Calculator();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void soma() {
        System.out.println("soma");

        assertEquals(3, s.soma(1,2));
        assertEquals(1, s.soma(1,0));
        assertEquals(0, s.soma(1,-1));
        assertEquals(0, s.soma(1000000000,-1000000000));
        assertEquals(-3, s.soma(-1,-2));
    }

    @Test
    void mulp1() {
        System.out.println("multipl");
        Calculator s=new Calculator();
        assertEquals(2, s.multipl(1,2));
        assertEquals(0, s.multipl(1,0));
        assertEquals(-1, s.multipl(1,-1));
        assertEquals(-1000000000, s.multipl(1,-1000000000));
        assertEquals(2, s.multipl(-1,-2));
    }

    @Test
    void firstbig1() {
        System.out.println("multipl");
        Calculator b=new Calculator();
        assertEquals(10, b.bigger(10,2));
    }

    @Test
    void firstbig2() {
        System.out.println("multipl");
        Calculator b=new Calculator();
        assertEquals(20, b.bigger(10,20));
    }

}