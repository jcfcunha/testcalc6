import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DivideTest {

    Divide div;

    @BeforeEach
    void setUp() {
        div = new Divide();
    }
    @Test
    void divide() {
        assertEquals(2.5, div.divide(5,2), 0.01);
    }
    @Test
    void divide0() {
        assertEquals(Double.POSITIVE_INFINITY, div.divide(5,0));
    }


}